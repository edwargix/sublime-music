"""
This module defines a stateless server which interops with the Subsonic API.
"""
from .server import Server

__all__ = ('Server', )
